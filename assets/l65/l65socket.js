var bgSocket =
{
	WebSocket : null,

	host : null,

	connect : function (host)
	{
		if( !('WebSocket' in window) )
		{
			console.log(	"[BGClient] Connection failed: WebSocket not " +
							"supported by browser" );
			return false;
		}

		console.log('[BGClient] Trying to connect to ' + host + '...');

		this.WebSocket = new WebSocket( host );
		this.WebSocket.binaryType = 'blob'; 

		this.WebSocket.onopen = this.onOpen;
		this.WebSocket.onmessage = this.onMessage;
		this.WebSocket.onerror = this.onError;
		this.WebSocket.onclose = this.onClose;

		window.onbeforeunload = this.onBeforeUnload;

		return true;

	},

	// <-----------------WebSocket Functions ---------------------------------->

	onOpen : function( evt ) 
	{
		console.log( "[BGClient] Connection opened" );
		console.log('[BGClient] protocol: ' + bgSocket.WebSocket.protocol);


	},

	onMessage : function (evt) 
	{  
		var msg = evt.data;
		console.log( '[BGClient] reveived data: ' + msg );

		//Call "cmd wrapper" with message
		var cmd = msg.split("\037");
		bgClient.callCmd( cmd );

	},

	onError : function( evt )
	{
		console.log( "[BGClient] Error Code: " + evt.code );
		console.log( evt );
	},

	onClose : function (evt) 
	{
		console.log( "[BGClient] Socket closing" );
		console.log( "[BGClient] Reason: " + evt.reason + " Code: " + evt.code );
		console.log( "[BGClient] Clean Close: " + evt.wasClean );
	},

	// <-----------------bgServer Functions ---------------------------------->

	sendMsg : function ( msg )
	{
		console.log( "[BGClient] Try Sending message..." + msg );
		
		this.WebSocket.send( msg );
		 //if ( this.WebSocket.readyState == 1 ) 
		 //{
		 //	console.log( '[BGClient] Sent message ' + msg );
		// 	this.WebSocket.send( msg );
		// }
		// else 
		// {
		 //	console.log( '[BGClient] Message not sent.' );
		// }

	},
	
	sendCmd : function ( )
	{
		console.log( '[BGClient] Try sending cmd...' );

		var cmd = arguments[0];

		for (var args = 1; args < arguments.length; args++)
		{
			cmd += ( '\037' + arguments[args] );
		}
		
		if ( this.WebSocket.readyState == socketState.OPEN ) 
		{
			console.log( '[BGClient] Sent cmd ' + cmd + '...' );
			this.WebSocket.send( cmd );
		}
		else 
		{
			console.log( '[BGClient] cmd ' + cmd + ' not sent.' );
		}

	},

	onBeforeUnload : function(e)
	{
		var evt = e || window.event;

		//send a preemptive close message before the inevitable null message
		// null cancels event
		bgSocket.sendMsg("test");

	    // For IE and Firefox prior to version 4
	    if (evt) {
	        evt.returnValue = null;//'Are you sure you want to close/refresh?';
	    }

	    // For Safari, chrome
	    return null;//'Are you sure you want to close/refresh?';
	}

};

var socketState =
{
	CONNECTING		: 0,
	OPEN			: 1,
	CLOSING			: 2,
	CLOSED			: 3
};
