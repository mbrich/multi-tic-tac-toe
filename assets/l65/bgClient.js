var bgClient =
{
	cmdList : [],
	entityList : [],
	gameStarted : false,

	// <------------------------- cmd Functions --------------------------->
	connect : function()
	{
		if (bgSocket.connect(BGSHost))
		{
			return true;
		}
		return false;
	},

	callCmd : function( cmd )
	{
		if ( this.cmdList[ cmd[0] ] !== undefined )
		{
			this.cmdList[ cmd[0] ](cmd);
		}
		else
		{
			console.log('[BGClient] Error: cmd ' + cmd[0] + ' is not valid');
		}
	},

	registerCmd : function(cmdID, cmd)
	{
		if ( this.cmdList[ cmdID ] === undefined )
		{
			this.cmdList[ cmdID ] = cmd;
		}
		else
		{
			console.log('[BGClient] Error: cmd' + cmdID + 
						' exists in cmdList');
		}
	},

	removeCmd : function(cmdID)
	{
		if ( this.cmdList[ cmdID ] === undefined )
		{
			this.cmdList.splice(cmdID, 1);
		}
		else
		{
			console.log('[BGClient] Error: cmd does not' + 
						'exist in cmdList');
		}
	}

};

var ClientState = 
{
	OPENING_HANDSHAKE : 0,
	OPEN : 1,
	CLOSING_HANDSHAKE : 2

};


