/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

var L65Options =
{

	outputTrace : true,
	outputDebug : true,
	outputInfo : true,
	outputWarning : true,
	outputError : true,
	logTrace : true,
	logDebug : true,
	logInfo : true,
	logWarning : true,
	logError : true,

}


var L65SessionState =
{
	LOBBY : 0,
	GAME : 1

};

var L65ConnectionState =
{
	CLOSED : 0,
	CONNECTING : 1,
	OPENING_HANDSHAKE : 2,
	AUTHENTICATING : 3,
	OPEN : 4,
	CLOSING_HANDSHAKE : 5


};


var L65Connection =
{
	state : L65ConnectionState.CLOSED,

	connect : function()
	{
		L65Socket.connect( L65Host );

	},

	serverHello : function()
	{
		L65Socket.sendEvent( L65EventCodes.SERVER_CLIENT_HELLO );

	},

	serverGoodbye : function()
	{
		L65Socket.sendEvent( L65EventCodes.SERVER_CLIENT_GOODBYE );

	}

};


function L65Session()
{
	entities : [];
	clients : [];
	maxClients : 0;
	minPlayers : 0;
	state = L65SessionState.LOBBY;

	registerClient = function()
	{

	};

	unregisterClient = function()
	{

	};

	registerEntity = function()
	{

	};

	unregisterEntity = function()
	{

	};


}


var L65Events =
{
	events : [],


	execute : function( fn )
	{
		if ( this.events[fn[0]] !== undefined )
		{
			this.events[fn[0]](fn);

		}
		else
		{
			if ( L65Options.outputError )
			{
				console.log( "Error: Event " + fn[0] + " is not registered." );

			}
		}
	},


	/**
	* 	Register a function in the network events list. A function cannot be
	*   called over the network until it has been registered.
	*/
	register : function( id, fn )
	{
		// if this function does not exist in our event list, then we add it as
		// a callback.
		if ( id === undefined || fn === undefined )
		{
			console.log( "Error: Event function or ID is undefined." );
			return;

		}

		if ( this.events[id] === undefined )
		{
			this.events[id] = fn;

		}
		else
		{
			if ( L65Options.outputDebug ) 
			{
				console.log( "Error: Event ID {" + id + "} already exists." );

			}
		}

	},


	/**
	*    Unregisters a function frmo the network events list.  A function cannot
	*    be called over the network once it has been unregistered.
	*/
	unregister : function (id )
	{
		if ( this.events[id] !== undefined )
		{
			this.events.splice( id, 1 );
		}
		else
		{
			if ( L65Options.outputError )
			{
				console.log( "Error: Event ID {" + id + "} does not exist" );

			}

		}

	}

};


var L65Socket =
{
	WebSocket : null,

	host : null,

	connect : function (host)
	{
		
		if( !('WebSocket' in window) )
		{
			console.log(	"Connection failed: WebSocket not " +
							"supported by browser" );
			return false;
		}

		if ( L65Options.outputInfo )
		{
				console.log('[BGClient] Trying to connect to ' + host + '...');

		
		}
		
		// Change the state to connecting.
		L65Session.connectionState = L65ConnectionState.CONNECTING;

		this.WebSocket = new WebSocket( host );
		this.WebSocket.binaryType = 'blob'; 

		this.WebSocket.onopen = this.onOpen;
		this.WebSocket.onmessage = this.onMessage;
		this.WebSocket.onerror = this.onError;
		this.WebSocket.onclose = this.onClose;

		window.onbeforeunload = this.onBeforeUnload;

		return true;

	},

	// <-----------------WebSocket Functions ---------------------------------->

	onOpen : function( evt ) 
	{
		L65Session.state = L65SessionState.AUTHENTICATING;
		
		

		console.log( "[BGClient] Connection opened" );
		L65Connection.serverHello();

	},

	onMessage : function (evt) 
	{  
		var msg = evt.data;


		if ( L65Options.outputTrace ) 
		{
			console.log( 'Reveived data: ' + msg );
		
		}
		
		//Call "cmd wrapper" with message
		var cmd = msg.split("\037");
		L65Events.execute( cmd );

	},

	onError : function( evt )
	{
		console.log( "[BGClient] Error Code: " + evt.code );
		console.log( evt );
	},

	onClose : function (evt) 
	{
		L65Session.connectionState = L65ConnectionState.CLOSED;
		console.log( "[BGClient] Socket closing" );
		console.log( "[BGClient] Reason: " + evt.reason + " Code: " + evt.code );
		console.log( "[BGClient] Clean Close: " + evt.wasClean );
	},

	// <-----------------bgServer Functions ---------------------------------->

	sendMsg : function ( msg )
	{
		console.log( "[BGClient] Try Sending message..." + msg );
		
		this.WebSocket.send( msg );
		 //if ( this.WebSocket.readyState == 1 ) 
		 //{
		 //	console.log( '[BGClient] Sent message ' + msg );
		// 	this.WebSocket.send( msg );
		// }
		// else 
		// {
		 //	console.log( '[BGClient] Message not sent.' );
		// }

	},
	
	sendEvent : function ( )
	{
		if ( L65Options.outputDebug ) 
		{
			console.log( 'Trying to send event...' );
		
		}
		
		var cmd = arguments[0];

		for (var args = 1; args < arguments.length; args++)
		{
			cmd += ( '\037' + arguments[args] );

		}
		
		if ( this.WebSocket.readyState == 1 ) 
		{
			this.WebSocket.send( cmd );
			console.log( '[BGClient] Sent cmd ' + cmd + '...' );

		}
		else 
		{
			console.log( '[BGClient] cmd ' + cmd + ' not sent.' );

		}

	},

	onBeforeUnload : function(e)
	{
		var evt = e || window.event;

		//send a preemptive close message before the inevitable null message
		// null cancels event
		//L65Socket.sendMsg("test");

	    // For IE and Firefox prior to version 4
	    if (evt) 
	    {
	        evt.returnValue = null;//'Are you sure you want to close/refresh?';
	    
	    }

	    // For Safari, chrome
	    return null;//'Are you sure you want to close/refresh?';
	}

};




var L65EventCodes =
{
	// 0 - 99
	// Basic socket actions 
	messageToClent 			: 0,
	rcvCloseClient			: 1,
	sndCloseClient			: 2,

	// 100 - 999
	// JavaScript Event Support for entities
	rcvEntityKeypress 		: 100,
	rcvEntityKeyup			: 101,
	rcvEntityKeydown		: 102,
	rcvEntityDragstart		: 103,
	rcvEntityDragover		: 104,
	rcvEntityFocus			: 105,
	rcvEntityBlur			: 106,
	rcvEntityClick			: 107,
	rcvEntityDblClick		: 108,
	rcvEntityMouseover		: 109,
	rcvEntityMouseout		: 110,
	rcvEntityMousewheel 	: 111,

	sndPlayerKeypress		: 150, 
	sndPlayerKeyup			: 151, 
	sndPlayerKeydown		: 152, 
	sndPlayerDragstart		: 153, 
	sndPlayerDragover		: 154,
	sndPlayerFocus			: 155,
	sndPlayerBlur			: 156, 
	sndPlayerClick			: 157, 
	sndPlayerDblClick		: 158, 
	sndPlayerMouseover		: 159, 
	sndPlayerMouseout		: 160,
	sndPlayerMousewheel		: 161,

	// General Entity Updates
	rcvEntityPos			: 200,
	rcvRemoveEntity			: 201,
	rcvCreateEntity			: 202,
	rcvEntityHP				: 203,
	sndEntityStats			: 204,

	SESSION_GET_LIST		: 400,
	SESSION_CLIENT_JOINED	: 401,
	SESSION_CLIENT_LEFT		: 402,
	SESSION_SET_STATE		: 403,
	SESSION_GET_STATE		: 404,
	SESSION_CLIENT_HELLO	: 405,
	SESSION_CLIENT_GOODBYE	: 406,
	
	// 500 - 599
	// Player Functions
	SERVER_CLIENT_HELLO		: 449,
	SERVER_CLIENT_JOINED	: 450,
	SERVER_CLIENT_LEFT		: 451,
	SERVER_CLIENT_WELCOME	: 452,
	SERVER_CLIENT_GOODBYE	: 453,


	sndJoinWorld			: 500,
	rcvLoadWorld			: 501,
	sndleaveWorld			: 502,
	sndJoinLevel			: 503,
	rcvLoadLevel			: 504,
	sndPlayerHP				: 505,
	rcvPlayerHP				: 506,
	sndPlayerPos			: 507,
	rcvPlayerPos			: 508,
	sndPlayerStats			: 509,
	rcvInitPlayer			: 510,
	sndchangeLevel			: 511

	// 2000 - 32768
	// Developer cmds


};
