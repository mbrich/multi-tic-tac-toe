/*
 * Copyright (C) 2013 - 2014 Kayvan Boudai and Michael Brich
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

// <------------ Default Cmds -------------------------------------->
// 0 - 99
// Basic socket actions

var L65Functions = 
{
	// 0 - 99
	// Basic socket actions 
	messageToClent 	: function()
	{
		L65Socket.sendMsg(arguments[0]);
	},



	serverGoodbye : function()
	{

	},



	rcvCloseClient : function()
	{
		//Closing handshake
		if (L65Socket.WebSocket.readyState != socketState.CLOSING &&
			L65Socket.WebSocket.readyState != socketState.CLOSED )
		{
			console.log('[BGClient] Closing...');
			L65Socket.WebSocket.readyState = socketState.CLOSING;
			L65Socket.WebSocket.close(1000, "Closing correctly through Command");
			//ig.game.getEntitiesByType( EntityPlayer )[0].kill();
		}
	},
	sndCloseClient : function()
	{
		L65Socket.sendCmd( DefaultCmdCodes.sndCloseClient );
	
	},


	// 100 - 999
	// JavaScript Event Support for entities
	// args entityId, keycode
	rcvEntityKeypress : function(cmd)
	{
		L65Session.entityList[cmd[1]].virtualKeyboard.
		doEvent('keyPress', cmd[2]);
	},

	rcvEntityKeyup	: function(cmd)
	{
		L65Session.entityList[cmd[1]].virtualKeyboard.
		doEvent('keyUp', cmd[2]);
	},

	rcvEntityKeydown: function(cmd)
	{
		L65Session.entityList[cmd[1]].virtualKeyboard.
		doEvent('keyDown', cmd[2]);
	},

	rcvEntityDragstart: function(cmd)
	{
		L65Session.entityList[cmd[1]].virtualKeyboard.
		doEvent('dragStart', cmd[2]);
	},

	rcvEntityDragover: function(cmd)
	{
		L65Session.entityList[cmd[1]].virtualKeyboard.
		doEvent('dragOver', cmd[2]);
	},

	rcvEntityFocus	: function(cmd)
	{
		L65Session.entityList[cmd[1]].virtualKeyboard.
		doEvent('focus', cmd[2]);
	},

	rcvEntityBlur	: function(cmd)
	{
		L65Session.entityList[cmd[1]].virtualKeyboard.
		doEvent('blur', cmd[2]);
	},

	rcvEntityClick	: function(cmd)
	{
		L65Session.entityList[cmd[1]].virtualKeyboard.
		doEvent('click', cmd[2]);
	},

	rcvEntityDblClick: function(cmd)
	{
		L65Session.entityList[cmd[1]].virtualKeyboard.
		doEvent('dblClick', cmd[2]);
	},

	rcvEntityMouseover: function(cmd)
	{
		L65Session.entityList[cmd[1]].virtualKeyboard.
		doEvent('mouseOver', cmd[2]);
	},

	rcvEntityMouseout: function(cmd)
	{
		L65Session.entityList[cmd[1]].virtualKeyboard.
		doEvent('mouseOver', cmd[2]);
	},

	rcvEntityMousewheel: function(cmd)
	{
		L65Session.entityList[cmd[1]].virtualKeyboard.
		doEvent('mouseWheel', cmd[2]);
	},


	// JavaScript Event Support for player
	// arg keyCode
	sndPlayerKeypress: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerKeypress, entityId, keyCode);
	},

	sndPlayerKeyup	: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerKeyup, entityId, keyCode);
	},

	sndPlayerKeydown: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerKeydown, entityId, keyCode);
	},

	sndPlayerDragstart: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerDragstart, entityId, keyCode);
	},

	sndPlayerDragover: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerDragover, entityId, keyCode);
	},

	sndPlayerFocus	: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerFocus, entityId, keyCode);
	},

	sndPlayerBlur	: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerBlur, entityId, keyCode);
	},

	sndPlayerClick	: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerClick, entityId, keyCode);
	},

	sndPlayerDblClick: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerDblClick, entityId, keyCode);
	},

	sndPlayerMouseover: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerMouseover, entityId, keyCode);
	},

	sndPlayerMouseout: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerMouseout, entityId, keyCode);
	},

	sndPlayerMousewheel: function(entityId, keyCode)
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndPlayerMousewheel, entityId, keyCode);
	},

	// General Entity Updates
	rcvEntityPos	: function()
	{

	},

	rcvRemoveEntity	: function(cmd)
	{
		for (var i = 0; i < ig.game.entities.length; i++)
		{
		 	if ( ig.game.entities[i].EntityID == cmd[1] )
		 	{
		 		ig.game.entities[i].kill();
		 		break;
		 	}
		}
	},

	rcvCreateEntity	: function( cmd )// [Cmd, EntityID, X, Y]
	{
		L65Session.entityList[cmd[1]] = 
		ig.game.spawnEntity(EntityPlayer2, 152, 88, {EntityID : cmd[1]});

	},

	rcvEntityHP		: function()
	{

	},

	sndEntityStats	: function()
	{

	},

	// 500 - 599
	// Player Functions
	sndJoinWorld	: function()
	{
		bgSocket.sendCmd( DefaultCmdCodes.sndJoinWorld );
	},

	rcvLoadWorld	: function()
	{

	},

	sndleaveWorld	: function()
	{
		//Close socket and other closing events

	},

	sndJoinLevel	: function()
	{

	},

	rcvLoadLevel	: function()
	{

	},

	sndPlayerHP		: function()
	{

	},

	rcvPlayerHP		: function()
	{

	},

	sndPlayerPos	: function()  // [x, y, socket]
	{
		L65Socket.sendCmd(	DefaultCmdCodes.sndPlayerPos,
							arguments[0], arguments[1] );
	},

	rcvPlayerPos	: function()
	{

	},

	sndPlayerStats	: function()
	{

	},

	rcvInitPlayer	: function(cmd) // [Cmd, EntityID, X, Y]
	{
		L65Session.entityList[cmd[1]] = 
		ig.game.spawnEntity(EntityPlayer, 152, 88, {EntityID : cmd[1]});
	},

	sndchangeLevel	: function()
	{

	}

	// 2000 - 32768
	// Developer cmds
	
};



L65Events.register( L65EventCodes.SERVER_HELLO, L65Functions.serverHello );
L65Events.register( L65EventCodes.SESSION_HELLO, L65Functions.sessionHello );

L65Events.register(L65EventCodes.messageToClent, L65Functions.messageToClent );
L65Events.register(L65EventCodes.rcvCloseClient, L65Functions.rcvCloseClient );
L65Events.register(L65EventCodes.sndCloseClient, L65Functions.sndCloseClient );


// 100 - 999
// JavaScript Event Support for entities
L65Events.register(L65EventCodes.rcvEntityKeypress, 
	L65Functions.rcvEntityKeypress );
L65Events.register(L65EventCodes.rcvEntityKeyup,
					L65Functions.rcvEntityKeyup );

L65Events.register(L65EventCodes.rcvEntityKeydown,
					L65Functions.rcvEntityKeydown );

L65Events.register(L65EventCodes.rcvEntityDragstart,
					L65Functions.rcvEntityDragstart );

L65Events.register(L65EventCodes.rcvEntityDragover,
					L65Functions.rcvEntityDragover );

L65Events.register(L65EventCodes.rcvEntityFocus,
					L65Functions.rcvEntityFocus );

L65Events.register(L65EventCodes.rcvEntityBlur,
					L65Functions.rcvEntityBlur );

L65Events.register(L65EventCodes.rcvEntityClick,
					L65Functions.rcvEntityClick );

L65Events.register(L65EventCodes.rcvEntityDblClick,
					L65Functions.rcvEntityDblClick );

L65Events.register(L65EventCodes.rcvEntityMouseover,
					L65Functions.rcvEntityMouseover );

L65Events.register(L65EventCodes.rcvEntityMouseout,
					L65Functions.rcvEntityMouseout );

L65Events.register(L65EventCodes.rcvEntityMousewheel,
					L65Functions.rcvEntityMousewheel );


L65Events.register(L65EventCodes.sndPlayerKeypress,
					L65Functions.sndPlayerKeypress );

L65Events.register(L65EventCodes.sndPlayerKeyup	,
					L65Functions.sndPlayerKeyup );

L65Events.register(L65EventCodes.sndPlayerKeydown,
					L65Functions.sndPlayerKeydown );

L65Events.register(L65EventCodes.sndPlayerDragstart,
					L65Functions.sndPlayerDragstart );

L65Events.register(L65EventCodes.sndPlayerDragover,
					L65Functions.sndPlayerDragover );

L65Events.register(L65EventCodes.sndPlayerFocus,
					L65Functions.sndPlayerFocus );

L65Events.register(L65EventCodes.sndPlayerBlur,
					L65Functions.sndPlayerBlur );

L65Events.register(L65EventCodes.sndPlayerClick,
					L65Functions.sndPlayerClick );

L65Events.register(L65EventCodes.sndPlayerDblClick,
					L65Functions.sndPlayerDblClick );

L65Events.register(L65EventCodes.sndPlayerMouseover,
					L65Functions.sndPlayerMouseover );

L65Events.register(L65EventCodes.sndPlayerMouseout,
					L65Functions.sndPlayerMouseout );

L65Events.register(L65EventCodes.sndPlayerMousewheel,
					L65Functions.sndPlayerMousewheel );


// General Entity Updates
L65Events.register(L65EventCodes.rcvEntityPos, L65Functions.rcvEntityPos );
L65Events.register(L65EventCodes.rcvRemoveEntity, L65Functions.rcvRemoveEntity );
L65Events.register(L65EventCodes.rcvCreateEntity, L65Functions.rcvCreateEntity );
L65Events.register(L65EventCodes.rcvEntityHP, L65Functions.rcvEntityHP );
L65Events.register(L65EventCodes.sndEntityStats, L65Functions.sndEntityStats );


// 500 - 599
// Player Functions
L65Events.register(L65EventCodes.sndJoinWorld, L65Functions.sndJoinWorld );
L65Events.register(L65EventCodes.rcvLoadWorld, L65Functions.rcvLoadWorld );
L65Events.register(L65EventCodes.sndleaveWorld, L65Functions.sndleaveWorld );
L65Events.register(L65EventCodes.sndJoinLevel, L65Functions.sndJoinLevel );
L65Events.register(L65EventCodes.rcvLoadLevel, L65Functions.rcvLoadLevel );
L65Events.register(L65EventCodes.sndPlayerHP, L65Functions.sndPlayerHP );
L65Events.register(L65EventCodes.rcvPlayerHP,L65Functions.rcvPlayerHP );
L65Events.register(L65EventCodes.sndPlayerPos, L65Functions.sndPlayerPos );
L65Events.register(L65EventCodes.rcvPlayerPos, L65Functions.rcvPlayerPos );
L65Events.register(L65EventCodes.sndPlayerStats,L65Functions.sndPlayerStats );
L65Events.register(L65EventCodes.rcvInitPlayer, L65Functions.rcvInitPlayer );
L65Events.register(L65EventCodes.sndchangeLevel, L65Functions.sndchangeLevel );





