var Game =
{

	currentTurnNum : 0,

	boardData : [ 
		[ { "sprite" : null, "value" : "e", "x" : 133, "y" : 123 },
		  { "sprite" : null, "value" : "e", "x" : 236, "y" : 123 },
		  { "sprite" : null, "value" : "e", "x" : 340, "y" : 123 }
		 ],
		[ { "sprite" : null, "value" : "e", "x" : 133, "y" : 223 },
		  { "sprite" : null, "value" : "e", "x" : 236, "y" : 223 },
		  { "sprite" : null, "value" : "e", "x" : 340, "y" : 223 }
		],
		[ { "sprite" : null, "value" : "e", "x" : 133, "y" : 323 },
		  { "sprite" : null, "value" : "e", "x" : 236, "y" : 323 },
		  { "sprite" : null, "value" : "e", "x" : 340, "y" : 323 }
		]

	],

	start : function()
	{

		console.log( "Game Starting..." );
		this.setupInputs();	
		this.drawBg();
		this.setupPieces();
		L65Connection.connect();	


	},


	clickPiece : function()
	{


	},


	drawBg : function()
	{
		var bgSprite = new Sprite();
		bgSprite.image = Textures.load( "assets/backgrounds/board_bg.jpg" );
		bgSprite.width = 540;
		bgSprite.height = 444;
		world.addChild( bgSprite );

		bgSprite.update = function()
		{
			if ( gInput.rightclick ) { alert( "left" ) }


		}

	},


	setupInputs : function()
	{
		console.log( "Setting inputs" );
		gInput.addBool( 252, "leftclick" );
		gInput.addBool( 254, "rightclick" );


	},


	setupPieces : function()
	{
		for ( var i = 0; i < 3; ++i )
		{
			for ( var j = 0; j < 3; ++j )
			{
				var gamePiece = new Sprite();
				gamePiece.width = 75;
				gamePiece.height = 75;
				gamePiece.x = this.boardData[i][j].x;
				gamePiece.y = this.boardData[i][j].y;
				this.boardData[i][j].sprite = gamePiece;
				gamePiece.image = Textures.load( "assets/sprites/o.jpg" );
				world.addChild( gamePiece );
				console.log( "Creating piece" );

			}
		}
	}


};